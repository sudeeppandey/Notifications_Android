package pandey.sudeep.notification;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.RemoteInput;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import static android.app.Notification.EXTRA_NOTIFICATION_ID;
import static android.app.NotificationManager.IMPORTANCE_DEFAULT;
import static android.app.NotificationManager.IMPORTANCE_HIGH;

public class MainActivity extends AppCompatActivity {

    private static String CHANNEL_ID = "1";
    static String CHANNEL_ID_II = "2";
    private Context context = this;
    NotificationChannel channel;
    NotificationChannel channel_II;
    static final String KEY_TEXT_REPLY = "key_text_reply";
    RemoteInput remoteInput;
    static String MyID = "test";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        myToolbar.setSubtitle("Gitlab Repo");
        myToolbar.setLogo(R.drawable.ic_lightbulb_outline_black_24dp);

        //final NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        final NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Create the NotificationChannel, but only on API 26+ because
            //the NotificationChannel class is new and not in the support library
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            channel = new NotificationChannel(CHANNEL_ID, name, IMPORTANCE_HIGH);
            channel.setDescription(description);
            // Register the channel with the system
            manager.createNotificationChannel(channel);

            CharSequence name_II = getString(R.string.channel_name_II);
            String description_II = getString(R.string.channel_description_II);
            channel_II = new NotificationChannel(CHANNEL_ID_II, name_II,IMPORTANCE_DEFAULT);
            channel.setDescription(description_II);
            manager.createNotificationChannel(channel_II);

        }

        Button basicNotificationButton = (Button)(findViewById(R.id.buttonforbasic));
        basicNotificationButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                 NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                        .setContentTitle("Notifying U")
                        .setContentText("First notification..")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                 manager.notify(1, mBuilder.build());
            }
        });

        Button bigText_basicNotificationButton = (Button)(findViewById(R.id.bigText_button));
        bigText_basicNotificationButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID_II)
                        .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                        .setContentTitle("Notifying U With Big Text")
                        .setContentText("Second Notification Message. Drag down to open..")
                        //for larger text area and content....
                        .setStyle(new NotificationCompat.BigTextStyle()
                              .bigText("Much longer text that cannot fit one line. If it's Oreo, then this" +
                                      "notification will be associated with second channel"))
                        .setPriority(NotificationCompat.PRIORITY_HIGH);
                manager.notify(2, mBuilder.build());
            }
        });

        Button tapActionButton = (Button)(findViewById(R.id.tapActionButton));
        tapActionButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                // Create an explicit intent for an Activity in your app
                Intent intent = new Intent(context, PendingResult.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                        .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                        .setContentTitle("Third Notification")
                        .setContentText("Tap the notification..")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        // Set the intent that will fire when the user taps the notification
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);
                manager.notify(3, mBuilder.build());
            }
        });

        Button action_button = (Button)(findViewById(R.id.actionButton));
        action_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                Intent actionIntent = new Intent(context, MyReceiver.class);
                actionIntent.setAction("ACTION_ACT");
                actionIntent.putExtra(EXTRA_NOTIFICATION_ID, 4);
                PendingIntent actionPendingIntent =
                        PendingIntent.getBroadcast(context, 0, actionIntent, 0);

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID_II)
                        .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                        .setContentTitle("Action notification")
                        .setContentText("initiates Broadcast Receiver")
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        //.setContentIntent(pendingIntent)
                        .addAction(R.drawable.ic_explore_black_24dp, getString(R.string.action),
                                actionPendingIntent);
                manager.notify(4,mBuilder.build());
            }
        });

        final Button replyButton = (Button)(findViewById(R.id.replyButton));
        replyButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {

                String replyLabel = getResources().getString(R.string.reply_label);
                remoteInput = new RemoteInput.Builder(KEY_TEXT_REPLY)
                        .setLabel(replyLabel)
                        .build();

                Intent replyIntent = new Intent(context, ReplyReceiver.class);
                replyIntent.setAction("ACTION_REPLY");
                replyIntent.putExtra(MyID,10);
                PendingIntent replyPendingIntent =
                        PendingIntent.getBroadcast(context,
                                5,
                                replyIntent,
                                0);

                // Create the reply action and add the remote input.
                NotificationCompat.Action action =
                        new NotificationCompat.Action.Builder(R.drawable.ic_explore_black_24dp,
                                getString(R.string.reply_label), replyPendingIntent)
                                .addRemoteInput(remoteInput)
                                .build();

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID_II)
                        .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                        .setContentTitle("Reply Notification")
                        .setContentText("User can directly type here")
                        .addAction(action);
                manager.notify(5, mBuilder.build());
            }



        });

        Button progressButton = (Button)findViewById(R.id.buttonProgress);
        progressButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v){

                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, CHANNEL_ID);
                mBuilder.setContentTitle("Notification Progress")
                        .setContentText("Shows Progress Bar along with task progress..")
                        .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                        .setPriority(NotificationCompat.PRIORITY_LOW);

                // Issue the initial notification with zero progress
                int PROGRESS_MAX = 100;
                int PROGRESS_CURRENT = 0;
                mBuilder.setProgress(PROGRESS_MAX, PROGRESS_CURRENT, false);
                manager.notify(6, mBuilder.build());

                for (int i=0;i<100;i++) {
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {

                    }
                    mBuilder.setProgress(PROGRESS_MAX, i+25, false);
                    manager.notify(6,mBuilder.build());
                    i=i+25;
                }
                mBuilder.setContentText("Done")
                        .setProgress(0,0,false);
                manager.notify(6, mBuilder.build());
            }
        });
    }
}
