package pandey.sudeep.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.RemoteInput;
import android.widget.Toast;

public class ReplyReceiver extends BroadcastReceiver {

    NotificationManagerCompat manager;
    @Override
    public void onReceive(Context context, Intent intent) {

        String id = intent.getExtras().get(MainActivity.MyID).toString();
        CharSequence chars = null;
        Bundle remoteInput = RemoteInput.getResultsFromIntent(intent);
        if (remoteInput != null) {
            chars = remoteInput.getCharSequence(MainActivity.KEY_TEXT_REPLY);
        }else{
            chars = null;
        }

        Toast toast = Toast.makeText(context, chars+"..Notification: "+id,Toast.LENGTH_LONG);
        toast.show();

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context,MainActivity.CHANNEL_ID_II)
                .setSmallIcon(R.drawable.ic_lightbulb_outline_black_24dp)
                .setContentText("Replied..");
         manager = NotificationManagerCompat.from(context);
         manager.notify(5,builder.build());

    }
}
