package pandey.sudeep.notification;

import android.app.Notification;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        String notificationID = intent.getExtras().get(Notification.EXTRA_NOTIFICATION_ID).toString();
        Toast toast = Toast.makeText(context, "BroaccastReceiver in action: "+notificationID,Toast.LENGTH_SHORT);
        toast.show();

    }
}
